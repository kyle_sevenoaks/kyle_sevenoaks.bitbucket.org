jQuery(document).ready(function() {
				var w = $.jWindow({ parentElement: '#window-container', id: 'phoneport', title: 'Resolution window', posx: 300, posy: 250, width: 360, height: 480, type: 'iframe', url: '', refreshButton: false, 
			minimiseButton: false,
			maximiseButton: false,
			closeButton: false })
						
				
				/*var a = $.jWindow({ id: 'phoneland', title: 'Smartphone Landscape', posx: 175, posy: 300, width: 490, height: 360, type: 'iframe', url: 'example/example.html' })*/
				

				
				var t = $.jWindow({ id: "tabletland", title: "Tablet Landscape", posx: 700, posy: 125, width: 1024, height: 768, type: 'iframe', tabs: true })
										.appendTab({ href: "documentation.html", title: "example/example.html" })
										.prependTab({ href: "example/example.html", title: "Example.com" })
										//.show();
										
										
												
				
				var modal = $.jWindow({ id: 'test-modal', title: 'Modal jWindow', posx: 400, posy: 100, width: 500, height: 300, type: 'iframe', url: 'example/example.html', modal: true });
				
				var windowCount = 0;
				
				
				
				var custHeight = $('#custHeight').attr('value');
				var custWidth = $('#custWidth').attr('value');
				
				//input dimension setting
				$("#custWidth").keyup(function () {
				      custWidth = $(this).attr('value');
				    }).keyup();
				
				$("#custHeight").keyup(function () {
				      custHeight = $(this).attr('value');
				    });
				
				$('#custSet').click( function() {
				    w.set({
				    	height: custHeight, 
				    	width: custWidth
				    	});
				});
				
				//open, close, refresh, clear
				
				jQuery('.open').on({
					click: function() {
						w.show();
					}
				});
				
				jQuery('.close').on({
					click: function() {
						
						w.hide();
					}
				});
				
				jQuery('.refresh').on({
					click: function() {
						w.update();
						a.update();
					}
				});
				
				jQuery('.clear').on({
					click: function() {
						w.update(null);
						a.update(null);
					}
				});
				
				jQuery('.experiMental').on({
					click: function() {
						//new $.jWindow({}).show();
						t.show();
					}
				});
				
				
				//get url button (this is a demo only, it ain't gonna work yet!')
				
				jQuery('#getUrl').on({
					click: function() {
						var custUrl = $('#urlInput').val();
						// console.log('before:' + custUrl);
						custUrl = custUrl.replace("https://", "");
						// console.log('after http strip:' + custUrl);
						// 	custUrl = custUrl.replace("www.", "");
						// console.log('after www strip:' + custUrl);
						w.update('https://' + custUrl);
						//w.update();
					}
				});
				
				//change frame dimensions
				
				jQuery('#ph-po').click( function() {
    					w.set({
    						height: '480',
    						width: '360'
    					});
				});
				
				jQuery('#ph-la').click( function() {
    					w.set({
    						height: '360',
    						width: '480'
    					});
				});
				
				jQuery('#ta-po').click( function() {
    					w.set({
    						height: '1024',
    						width: '768'
    					});
				});
				
				jQuery('#ta-la').click( function() {
    					w.set({
    						height: '768',
    						width: '1024'
    					});
				});
				
				//draggable and stuff
				
				jQuery('.draggable').on({
					change: function() {
						w.set('draggable',jQuery(this).is(':checked'));
					}
				});
				
				jQuery('.resizeable').on({
					change: function() {
						w.set('resizeable',jQuery(this).is(':checked'));
					}
				});
				
				jQuery('.new').on({
					click: function() {
						++windowCount;
						var w = $.jWindow({ id: 'test-'+windowCount, title: 'Reslution Window '+windowCount, posx: 200, posy: 100, width: 500, height: 300, type: 'iframe' }).show();
					}
				});
				
				jQuery('.modal').on({
					click: function() {
						modal.show();
					}
				});
				
				jQuery('.theme').on({
					click: function() {
						var theme = jQuery(this).attr('rel');
						jQuery('link').each(function() {
							if (jQuery(this).attr('title') == 'theme') {
								jQuery(this).attr('href','themes/' + theme + '/style.css')
							}
						});
					}
				});
				
				jQuery('.tabadd').on({
					click: function() {
						t.appendTab({ href: "https://google.com", title: "Umbrarum Regnum", name: "ur" });
					}
				});
				
				jQuery('.tabopen').on({
					click: function() {
						t.openTab("ur");
					}
				});
				
				jQuery('.tabclose').on({
					click: function() {
						t.closeTab("ur");
					}
				});
			});
